package com.n3k0.tictactoe.viewmodel;

/**
 * Created by N3k0 on 08/14/17.
 */
interface ViewModel {

    void onCreate();
    void onPause();
    void onResume();
    void onDestroy();

}
